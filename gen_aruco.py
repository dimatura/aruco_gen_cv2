import os
import subprocess
from glob import glob

import cv2
import cv2.aruco as aruco

DIRNAME = './tags'
ARUCODICT = aruco.DICT_6X6_250
PX_SIZE = 700
N_TAGS = 250

os.mkdir(DIRNAME)

aruco_dict = aruco.Dictionary_get(ARUCODICT)
for i in range(N_TAGS):
    img = aruco.drawMarker(aruco_dict, i, PX_SIZE)
    cv2.imwrite('%s/marker_%03d.png'%(DIRNAME, i), img)

doc = """\
\documentclass{article}
    % General document formatting
    \usepackage[margin=0.7in]{geometry}
    \usepackage[parfill]{parskip}
    \usepackage[utf8]{inputenc}
    \usepackage{graphicx}
    \\begin{document}
"""

fig = """\\begin{{figure}}[h]
\centering
\caption{{{tagid}}}
\includegraphics[width=1.0\\textwidth]{{{img}}}
\end{{figure}}
\\newpage
"""

for i, img in enumerate(sorted(glob('%s/*.png'%DIRNAME))):
    fig2 = fig.format(img=img, tagid='tag %02d'%i)
    doc += ('\n' + fig2 + '\n')
doc += '\\end{document}'

with open('tags.tex', 'w') as f:
    f.write(doc)

subprocess.call(['pdflatex', 'tags.tex'])
